Présentation
============

PasseGares est une application android ayant pour contexte les gares et stations
de transport en communs. Il permet de lister l'ensemble des gares auquel on est 
passé, en les tamponnant quand on se trouve à moins de 150m d'elles.

Une partie jeu de gestion est en cours de création autour de ce concept.

Elle ne fonctionne qu'avec les gares de la région Île-de-France, en France pour
le moment. Néanmoins, vous pouvez aider à l'intégration de données pour d'autres
régions.

Installation
============

Cette application est compilé avec android-studio.

Contribution
============

Cette application est ouverte aux contributions.

Crédits
=======

Les logos et sources de données appartiennent à leur propriétaire respectifs, et
ne sont pas compris dans le cadre de la licence de ce logiciel. Vous pouvez les
retrouver dans le fichier Preference.java (le code est toujours à jour :) ).

Contact
=======

Vous pouvez poster une requête de bug, ou envoyer un courriel à :
passegares[@]nocle.fr