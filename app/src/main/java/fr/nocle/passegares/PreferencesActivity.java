package fr.nocle.passegares;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Message;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import fr.nocle.passegares.bdd.GareBDD;
import fr.nocle.passegares.bdd.GareDansLigneBDD;
import fr.nocle.passegares.bdd.InventaireBDD;
import fr.nocle.passegares.bdd.LigneBDD;
import fr.nocle.passegares.bdd.TamponBDD;
import fr.nocle.passegares.controlleur.Controlleur;
import fr.nocle.passegares.controlleur.GareCtrl;
import fr.nocle.passegares.controlleur.ImportCSV;

class ReinitThread extends Thread {
    private Context contexte;
    private DialogInterface loadingDialog;

    ReinitThread(Context context, DialogInterface loadingDialog) {
        this.contexte = context;
        this.loadingDialog = loadingDialog;
    }

    public void run() {
        Controlleur ctrl = new Controlleur(contexte);
        SQLiteDatabase bdd = ctrl.open();
        bdd.execSQL(TamponBDD.TABLE_SUPPRESSION);
        bdd.execSQL(GareBDD.TABLE_SUPPRESSION);
        bdd.execSQL(LigneBDD.TABLE_SUPPRESSION);
        bdd.execSQL(GareDansLigneBDD.TABLE_SUPPRESSION);
        bdd.execSQL(InventaireBDD.TABLE_SUPPRESSION);

        bdd.execSQL(TamponBDD.TABLE_CREATION);
        bdd.execSQL(GareBDD.TABLE_CREATION);
        bdd.execSQL(LigneBDD.TABLE_CREATION);
        bdd.execSQL(GareDansLigneBDD.TABLE_CREATION);
        bdd.execSQL(InventaireBDD.TABLE_CREATION);
        bdd.execSQL(InventaireBDD.TABLE_INIT);

        ImportCSV.updateData(contexte, bdd, 1, -1);

        ctrl.close();

        loadingDialog.dismiss();
//        Toast.makeText(contexte, "Réinitialisation terminée.", Toast.LENGTH_LONG).show();
    }
}

public class PreferencesActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            Preference viderBDDPref = (Preference) findPreference("viderBDD");
            //On fait une dialogue de confirmation
            final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

            //On créer aussi une dialogue de chargement
            dialog.setMessage(R.string.dialogVidageBDDExplication).setTitle(R.string.dialogVidageBDDTitre);
            dialog.setPositiveButton(R.string.boutonEffacer, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // On valide la suppression des données
                    dialog.cancel();
                    ProgressDialog loadingDialog = ProgressDialog.show(getActivity(), null, getActivity().getString(R.string.dialogVidageBDDSuppressionEnCours), true);

                    ReinitThread p = new ReinitThread(getActivity(), loadingDialog);
                    p.start();
                }
            });
            dialog.setNegativeButton(R.string.boutonAnnuler, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // On refuse la suppression des données
                    dialog.cancel();
                }
            });

            dialog.create();
            viderBDDPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    dialog.show();

                    return true;
                }
            });

            Preference versionAPKPref = (Preference) findPreference("versionAPK");
            String versionName = BuildConfig.VERSION_NAME;
            versionAPKPref.setSummary(versionName);

            Preference iconeApplication = (Preference) findPreference("iconeCredits");
            iconeApplication.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(getActivity(), CreditsActivity.class);

                    startActivity(i);
                    return true;
                }
            });
        }
    }
}
