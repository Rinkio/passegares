package fr.nocle.passegares.modele;

import fr.nocle.passegares.outils.CouleurOutils;

/**
 * Created by jonathanmm on 19/12/16.
 */

public class Ticket {
    private int couleur;
    private int nombre;

    public Ticket(int couleur, int nombre) {
        this.couleur = couleur;
        this.nombre = nombre;
    }

    public int getCouleur() {
        return couleur;
    }

    public void setCouleur(int couleur) {
        this.couleur = couleur;
    }

    public String getCouleurString()
    {
        return CouleurOutils.getHexa(this.couleur);
    }

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }
}
