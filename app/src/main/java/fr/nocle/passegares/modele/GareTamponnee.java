package fr.nocle.passegares.modele;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jonathanmm on 02/10/16.
 */

public class GareTamponnee {
    private String nomGare;
    private long idGare;
    private int nbValidations;
    private Date dateDerniereValidation;
    private int planDeLigneFond;
    private int planDeLignePoint;

    public static final int POINT_CENTRE_DEUX = 1;
    public static final int POINT_CENTRE_BAS = 2;
    public static final int POINT_CENTRE_HAUT = 3;
    public static final int POINT_DROITE_DEUX = 11;
    public static final int POINT_DROITE_BAS = 12;
    public static final int POINT_DROITE_HAUT = 13;
    public static final int POINT_GAUCHE_DEUX = 21;
    public static final int POINT_GAUCHE_BAS = 22;
    public static final int POINT_GAUCHE_HAUT = 23;

    public static final int LIGNE_SEUL_CENTRE = 1;
    public static final int LIGNE_SEUL_GAUCHE = 2;
    public static final int LIGNE_SEUL_DROITE = 3;
    public static final int LIGNE_SEUL_BRANCHE = 4;
    public static final int LIGNE_CENTRE_SEUL = 10;
    public static final int LIGNE_CENTRE_CENTRE = 11;
    public static final int LIGNE_CENTRE_BRANCHE = 14;
    public static final int LIGNE_GAUCHE_SEUL = 20;
    public static final int LIGNE_GAUCHE_GAUCHE = 22;
    public static final int LIGNE_GAUCHE_BRANCHE = 24;
    public static final int LIGNE_DROITE_SEUL = 30;
    public static final int LIGNE_DROITE_DROITE = 33;
    public static final int LIGNE_DROITE_BRANCHE = 34;
    public static final int LIGNE_BRANCHE_SEUL = 40;
    public static final int LIGNE_BRANCHE_CENTRE = 41;
    public static final int LIGNE_BRANCHE_GAUCHE = 42;
    public static final int LIGNE_BRANCHE_DROITE = 43;
    public static final int LIGNE_BRANCHE_BRANCHE = 44;

    public GareTamponnee(long idGare, String nomGare, int nbValidations, String date) {
        this.idGare = idGare;
        this.nomGare = nomGare;
        this.nbValidations = nbValidations;
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(date == null)
            this.dateDerniereValidation = null;
        else{
            try {
                this.dateDerniereValidation = formatDate.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public GareTamponnee(long idGare, String nomGare, int nbValidations, String date, int planDeLigneFond, int planDeLignePoint) {
        this(idGare, nomGare, nbValidations, date);
        this.planDeLigneFond = planDeLigneFond;
        this.planDeLignePoint = planDeLignePoint;
    }

    public long getIdGare() {
        return idGare;
    }

    public void setIdGare(long idGare) {
        this.idGare = idGare;
    }

    public String getNomGare() {
        return nomGare;
    }

    public void setNomGare(String nomGare) {
        this.nomGare = nomGare;
    }

    public int getNbValidations() {
        return nbValidations;
    }

    public void setNbValidations(int nbValidations) {
        this.nbValidations = nbValidations;
    }

    public Date getDateDerniereValidation() {
        return dateDerniereValidation;
    }

    public String getStringDateDerniereValidation() {
        if(dateDerniereValidation == null)
            return "";
        Date auj = new Date();
        long millisecondes = auj.getTime() - dateDerniereValidation.getTime();
        int secondes = Math.round(millisecondes / 1000);
        if(secondes < 2)
            return "Il y a "+secondes+" seconde";
        else if(secondes < 60)
            return "Il y a "+secondes+" secondes";
        else if(secondes < 120)
            return "Il y a 1 minute";
        else if(secondes < 3600)
            return "Il y a "+(int) Math.ceil(secondes/60)+" minutes";
        else if(secondes < 7200)
            return "Il y a 1 heure";
        else if(secondes < 86400)
            return "Il y a "+(int) Math.ceil(secondes/3600)+" heures";
        else if(secondes < 172800) //86400*2
            return "Hier";
        else if(secondes <= 604800) //86400*7
            return "Il y a "+(int) Math.ceil(secondes/86400)+" jours";
        else
        {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return formatDate.format(dateDerniereValidation);
        }
    }

    public void setDateDerniereValidation(Date dateDerniereValidation) {
        this.dateDerniereValidation = dateDerniereValidation;
    }

    public int getPlanDeLigneFond() {
        return planDeLigneFond;
    }

    public void setPlanDeLigneFond(int planDeLigneFond) {
        this.planDeLigneFond = planDeLigneFond;
    }

    public int getPlanDeLignePoint() {
        return planDeLignePoint;
    }

    public void setPlanDeLignePoint(int planDeLignePoint) {
        this.planDeLignePoint = planDeLignePoint;
    }
}
