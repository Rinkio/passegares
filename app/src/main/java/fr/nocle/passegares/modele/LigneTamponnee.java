package fr.nocle.passegares.modele;

/**
 * Created by jonathanmm on 01/10/16.
 */

public class LigneTamponnee {
    private long idLigne;
    private String nomLigne;
    private int nbTampons;
    private int nbGares;
    private int ordre;

    public LigneTamponnee(long id, String nomLigne, int nbTampons, int nbGares, int ordre) {
        this.idLigne = id;
        this.nomLigne = nomLigne;
        this.nbTampons = nbTampons;
        this.nbGares = nbGares;
        this.ordre = ordre;
    }

    public long getIdLigne() {
        return idLigne;
    }

    public void setIdLigne(long id) {
        this.idLigne = id;
    }

    public String getNomLigne() {
        return nomLigne;
    }

    public void setNomLigne(String nomLigne) {
        this.nomLigne = nomLigne;
    }

    public int getNbTampons() {
        return nbTampons;
    }

    public void setNbTampons(int nbTampons) {
        this.nbTampons = nbTampons;
    }

    public int getNbGares() {
        return nbGares;
    }

    public void setNbGares(int nbGares) {
        this.nbGares = nbGares;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }
}
