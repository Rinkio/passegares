package fr.nocle.passegares.modele;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jonathanmm on 02/09/16.
 */
public class Tampon {
    private long id;
    private long idGare;
    private Gare gare;
    private Date date;

    public Tampon(long id, long idGare, Date date)
    {
        super();
        initObj(id, idGare, date);
        this.gare = null;
    }

    public Tampon(long id, Gare gare, Date date)
    {
        super();
        initObj(id, gare.getId(), date);
        this.gare = gare;
    }

    public Tampon(long id, long idGare, String date)
    {
        super();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            initObj(id, idGare, formatDate.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.gare = null;
    }

    public Tampon(long id, Gare gare, String date)
    {
        super();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            initObj(id, gare.getId(), formatDate.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.gare = gare;
    }

    private void initObj(long id, long idGare, Date date)
    {
        this.id = id;
        this.idGare = idGare;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdGare() {
        return idGare;
    }

    public void setIdGare(long idGare) {
        this.idGare = idGare;
    }

    public Date getDate() {
        return date;
    }
    public String getDateString() {
        Date auj = new Date();
        long millisecondes = auj.getTime() - date.getTime();
        int secondes = Math.round(millisecondes / 1000);
        if(secondes < 2)
            return "Il y a "+secondes+" seconde";
        else if(secondes < 60)
            return "Il y a "+secondes+" secondes";
        else if(secondes < 120)
            return "Il y a 1 minute";
        else if(secondes < 3600)
            return "Il y a "+(int) Math.ceil(secondes/60)+" minutes";
        else if(secondes < 7200)
            return "Il y a 1 heure";
        else if(secondes < 86400)
            return "Il y a "+(int) Math.ceil(secondes/3600)+" heures";
        else if(secondes < 172800) //86400*2
            return "Hier";
        else if(secondes <= 604800) //86400*7
            return "Il y a "+(int) Math.ceil(secondes/86400)+" jours";
        else
        {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return formatDate.format(date);
        }
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Gare getGare() {
        return gare;
    }

    public void setGare(Gare gare) {
        this.gare = gare;
    }
}
