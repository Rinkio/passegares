package fr.nocle.passegares.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import fr.nocle.passegares.controlleur.GareCtrl;
import fr.nocle.passegares.controlleur.ImportCSV;
import fr.nocle.passegares.controlleur.LigneCtrl;

/**
 * Created by jonathanmm on 03/09/16.
 */
public class Connecteur extends SQLiteOpenHelper {
    private Context contexte;

    public Connecteur(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        contexte = context;
    }

    @Override
    public void onCreate(SQLiteDatabase bdd) {
        bdd.execSQL(TamponBDD.TABLE_CREATION);
        bdd.execSQL(GareBDD.TABLE_CREATION);
        bdd.execSQL(LigneBDD.TABLE_CREATION);
        bdd.execSQL(GareDansLigneBDD.TABLE_CREATION);
        bdd.execSQL(InventaireBDD.TABLE_CREATION);
        bdd.execSQL(InventaireBDD.TABLE_INIT);

        ImportCSV.updateData(contexte, bdd, 1, -1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase bdd, int oldVersion, int newVersion) {
        if(oldVersion < 50 && newVersion >= 63) //Accélération de la montée de niveau en stable
        {
            //Création des tables
            bdd.execSQL(InventaireBDD.TABLE_CREATION);

            //Ajouts de champs
            bdd.execSQL(LigneBDD.TABLE_ALTER_COULEUR);
            bdd.execSQL(LigneBDD.TABLE_ALTER_ORDRE);
            bdd.execSQL(GareBDD.TABLE_ALTER_COULEUR_EVO);
            bdd.execSQL(GareBDD.TABLE_ALTER_COULEUR);
            bdd.execSQL(GareBDD.TABLE_ALTER_NIVEAU);
            bdd.execSQL(GareDansLigneBDD.TABLE_ALTER_ORDRE);
            bdd.execSQL(GareDansLigneBDD.TABLE_ALTER_PLANDELIGNE_FOND);
            bdd.execSQL(GareDansLigneBDD.TABLE_ALTER_PLANDELIGNE_POINT);

            //Initialisation
            bdd.execSQL(InventaireBDD.TABLE_INIT);

            //Calcul des niveaux
            GareCtrl.updateAllLevels(contexte, bdd);
        } else {
            if(oldVersion <= 62 && newVersion >= 63)
            {
                bdd.execSQL(InventaireBDD.TABLE_SUPPRESSION);
                bdd.execSQL(InventaireBDD.TABLE_CREATION);
                bdd.execSQL(InventaireBDD.TABLE_INIT);
            }

            if(oldVersion <= 57 && newVersion >= 58)
            {
                bdd.execSQL(LigneBDD.TABLE_ALTER_COULEUR);
            }

            if(oldVersion <= 56 && newVersion >= 57)
            {
                bdd.execSQL(GareBDD.TABLE_ALTER_COULEUR_EVO);
            }

            if(oldVersion <= 54 && newVersion >= 55)
            {
                bdd.execSQL(GareDansLigneBDD.TABLE_ALTER_ORDRE);
                bdd.execSQL(GareDansLigneBDD.TABLE_ALTER_PLANDELIGNE_FOND);
                bdd.execSQL(GareDansLigneBDD.TABLE_ALTER_PLANDELIGNE_POINT);
            }

            if(oldVersion <= 53 && newVersion >= 54)
            {
                bdd.execSQL(InventaireBDD.TABLE_CREATION);
                bdd.execSQL(InventaireBDD.TABLE_INIT);
            }

            if(oldVersion <= 52 && newVersion >= 53)
            {
                bdd.execSQL(GareBDD.TABLE_ALTER_COULEUR);
            }

            if(oldVersion <= 51 && newVersion >= 52)
            {
                bdd.execSQL(GareBDD.TABLE_ALTER_NIVEAU);
                GareCtrl.updateAllLevels(contexte, bdd);
            }

            if(oldVersion <= 50 && newVersion >= 51) //Ajout de l'ordre dans les lignes
            {
                bdd.execSQL(LigneBDD.TABLE_ALTER_ORDRE);
            }
        }

        if(oldVersion <= 12 && newVersion >= 13) //Corruption dans les tampons
        {
            bdd.execSQL(TamponBDD.TABLE_SUPPRESSION);
            bdd.execSQL(TamponBDD.TABLE_CREATION);
        }

        if(oldVersion <= 10 && newVersion >= 11) //On réimporte de nouvelles données
        {
            bdd.execSQL(TamponBDD.TABLE_SUPPRESSION);
            bdd.execSQL(GareBDD.TABLE_SUPPRESSION);
            bdd.execSQL(LigneBDD.TABLE_SUPPRESSION);
            bdd.execSQL(GareDansLigneBDD.TABLE_SUPPRESSION);

            onCreate(bdd);
        }

        if(oldVersion <= 5 && newVersion >= 6) //On réimporte de nouvelles données
        {
            bdd.execSQL(TamponBDD.TABLE_SUPPRESSION);
            bdd.execSQL(GareBDD.TABLE_SUPPRESSION);

            onCreate(bdd);
        }

        if(oldVersion <= 4 && newVersion >= 5)
        {
            //Création de l'index longitude latitude
            bdd.execSQL("CREATE INDEX \"" + GareBDD.TABLE_NOM + "_main\" ON " + GareBDD.TABLE_NOM + " (" + GareBDD.TABLE_LONGITUDE + " ASC, " + GareBDD.TABLE_LATITUDE + " ASC)");
        }

        if(newVersion <= 4) {
            bdd.execSQL(TamponBDD.TABLE_SUPPRESSION);
            bdd.execSQL(GareBDD.TABLE_SUPPRESSION);

            onCreate(bdd);
        }

        if(newVersion >= 14) //À partir de cette version, on fait les mises à jour par CSV
        {
            ImportCSV.updateData(contexte, bdd, oldVersion, newVersion);
        }

        //On fait la mise à jour une fois qu'on a les bonnes couleurs
        if((oldVersion <= 52 && newVersion >= 53) || (oldVersion <= 53 && newVersion >= 54)|| (oldVersion <= 62 && newVersion >= 63))
        {
            GareCtrl.updateAllLevels(contexte, bdd);
        }

        //Fix problème avec GL
        if(oldVersion <= 61 && newVersion >= 62)
        {
            LigneCtrl.fixProblemeGL(contexte, bdd);
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //super.onDowngrade(db, oldVersion, newVersion);
    }
}

