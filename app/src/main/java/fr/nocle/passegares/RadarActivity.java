package fr.nocle.passegares;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import fr.nocle.passegares.adapter.GaresAdapter;
import fr.nocle.passegares.controlleur.GareCtrl;
import fr.nocle.passegares.controlleur.InventaireCtrl;
import fr.nocle.passegares.controlleur.TamponCtrl;
import fr.nocle.passegares.modele.Gare;
import fr.nocle.passegares.modele.Tampon;
import fr.nocle.passegares.outils.StringOutils;

class MessageHandler extends Handler {
    private RadarActivity mainActivity;
    private GareCtrl gareControlleur;
    private TamponCtrl tamponControlleur;
    private android.support.v4.app.NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;
    private ArrayList<Gare> listeGares;
    private InventaireCtrl inventaireControlleur;

    public MessageHandler(RadarActivity activity)
    {
        mainActivity = activity;
        gareControlleur = new GareCtrl(activity);
        tamponControlleur = new TamponCtrl(activity);
        inventaireControlleur = new InventaireCtrl(activity);
        mNotificationManager = null;
        mBuilder = null;
    }

    @Override
    public void handleMessage(Message message) {
        if(message.arg1 == 1)
        {
            int nomApplicationId = mainActivity.getApplicationInfo().labelRes;
            //On passe en mode notif !
            mBuilder =
                    new NotificationCompat.Builder(mainActivity)
                            .setContentTitle(mainActivity.getString(nomApplicationId));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                mBuilder.setSmallIcon(R.drawable.pg_logo);
            else
                mBuilder.setSmallIcon(R.drawable.pg_logo_png);

            if(listeGares != null && listeGares.size() > 0)
            {
                mBuilder.setContentText(StringOutils.displayBeautifullNameStation(listeGares.get(0).getNom())).setNumber((int) listeGares.get(0).getDistance());
            } else {
                mBuilder.setContentText(mainActivity.getString(R.string.aucuneGareProche)).setNumber(0);
            }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                Intent resultIntent = new Intent(mainActivity, RadarActivity.class);
                TaskStackBuilder stackBuilder = null;
                stackBuilder = TaskStackBuilder.create(mainActivity);
                stackBuilder.addParentStack(RadarActivity.class);
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                mBuilder.setContentIntent(resultPendingIntent);
                mNotificationManager = (NotificationManager) mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(0, mBuilder.build());
            }
        } else if(message.arg1 == 2)
        {
            if(mBuilder != null)
            {
                //On supprime les notifs !
                mNotificationManager.cancel(0);
                mBuilder = null;
                mNotificationManager = null;
                //Et on met à jour l'affichage au passage
                if(listeGares != null && listeGares.size() > 0)
                    mettreAJourAffichage(gareControlleur, listeGares.get(0), (float) listeGares.get(0).getDistance());
            }
        } else {
            Bundle recu = message.getData();
            Double lat = recu.getDouble("LAT");
            Double lon = recu.getDouble("LON");

            Location location = new Location("GPS");
            location.setLatitude(lat);
            location.setLongitude(lon);

            listeGares = gareControlleur.getNearlest(location);
            float minDistance = -1;
            Gare garePlusProche = null;

            for(Gare g : listeGares)
            {
                float distance = location.distanceTo(g.getLocation());
                g.setDistance(distance);
                if(minDistance == -1 || distance < minDistance)
                {
                    minDistance = distance;
                    garePlusProche = g;
                }
            };

            Collections.sort(listeGares, new Comparator<Gare>() {
                @Override
                public int compare(Gare o1, Gare o2) {
                    if(o1.getDistance() > o2.getDistance())
                        return 1;
                    else if(o1.getDistance() < o2.getDistance())
                        return -1;
                    else
                        return 0;
                }
            });

            if(mBuilder == null) {
                mettreAJourAffichage(gareControlleur, garePlusProche, minDistance);
            } else {
                mettreAJourNotif(garePlusProche, minDistance);
            }
        }
    }

    private void mettreAJourNotif(Gare garePlusProche, float minDistance)
    {
        //On met à jour la notif
        if (garePlusProche == null) {
            mBuilder.setContentText(mainActivity.getString(R.string.aucuneGareProche)).setNumber(0);
            // Because the ID remains unchanged, the existing notification is
            // updated.
            mNotificationManager.notify(
                    0,
                    mBuilder.build());
        } else {
            mBuilder.setContentText(StringOutils.displayBeautifullNameStation(garePlusProche.getNom()))
                    .setNumber((int) minDistance);
            /*//TODO : Le bouton Tamponner ne fait rien
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                if (minDistance <= 150 && !tamponControlleur.ifAlreadyTamponned(garePlusProche.getId()) && mBuilder.mActions.size() == 0) {

                    Intent resultIntent = new Intent(mainActivity, RadarActivity.class);
                    resultIntent.putExtra("Action", "Tamponner");
                    TaskStackBuilder stackBuilder = null;
                    stackBuilder = TaskStackBuilder.create(mainActivity);
                    stackBuilder.addParentStack(RadarActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent =
                            stackBuilder.getPendingIntent(
                                    0,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            );
                    //mBuilder.addAction(R.drawable.fa_book, mainActivity.getString(R.string.tamponner), resultPendingIntent);
                } else {
                    mBuilder.mActions.clear();
                }
            }*/
            mNotificationManager.notify(
                    0,
                    mBuilder.build());
        }
    }

    private boolean canIEvoluate(Gare garePlusProche)
    {
        int niveauCourant = garePlusProche.getNiveau();
        if(niveauCourant == 0)
            return tamponControlleur.getCountTampon(garePlusProche.getId()) >= 3;
        else if(niveauCourant == 1)
            return inventaireControlleur.getNbTicket(garePlusProche.getCouleurEvo()) >= 10;
        else if(niveauCourant == 2)
            return inventaireControlleur.getNbTicket(garePlusProche.getCouleurEvo()) >= 25;

        return false;
    }

    private void prepareDialogEvolution(View viewDialog, Gare garePlusProche, String labelEvolution)
    {
        TextView labelMessage = (TextView) viewDialog.findViewById(R.id.evolutionLabel);
        TextView nbTicket = (TextView) viewDialog.findViewById(R.id.nombreTicket);
        ImageView iconeTicket = (ImageView) viewDialog.findViewById(R.id.iconTicket);
        int color = Color.parseColor(garePlusProche.getCouleurEvoString());
        iconeTicket.setColorFilter(color);

        labelMessage.setText(labelEvolution);
        int nbTicketsNiveauSuivant = 0;
        if(garePlusProche.getNiveau() == 1)
            nbTicketsNiveauSuivant = 10;
        else if(garePlusProche.getNiveau() == 2)
            nbTicketsNiveauSuivant = 25;

        nbTicket.setText(String.valueOf(nbTicketsNiveauSuivant));
    }

    private void gestionBoutonEvolution(final GareCtrl gareControlleur, final Gare garePlusProche, final ImageView boutonEvolution, final float minDistance) {
        boutonEvolution.setVisibility(View.VISIBLE);
        boutonEvolution.setEnabled(true);

        //On va créer la boîte de dialogue pour l'évolution
        final AlertDialog.Builder dialog = new AlertDialog.Builder(mainActivity);
        dialog.setTitle(R.string.evoluerGare);

        //On regarde si la condition de changement de niveau est respectée
        if (canIEvoluate(garePlusProche))
        {
            String labelEvolution = "Voulez-vous vraiment faire passer la gare au niveau "+ (garePlusProche.getNiveau() + 1) + " ?";
            if(garePlusProche.getNiveau() == 0)
                dialog.setMessage(labelEvolution);
            else {
                LayoutInflater inflater = mainActivity.getLayoutInflater();

                View viewDialog = inflater.inflate(R.layout.dialog_evolution, null);
                prepareDialogEvolution(viewDialog, garePlusProche, labelEvolution);
                dialog.setView(viewDialog);
            }
            dialog.setPositiveButton(R.string.boutonValider, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   Gare g = gareControlleur.get(garePlusProche.getId());
                   g.setNiveau(g.getNiveau() + 1);
                   gareControlleur.update(g);

                    //On met aussi à jour la vue
                    listeGares.get(0).setNiveau(g.getNiveau());

                    //On prélève aussi les tickets
                    if(g.getNiveau() == 2)
                        inventaireControlleur.jeterTicket(garePlusProche.getCouleurEvo(), 10);
                    else if(g.getNiveau() == 3)
                        inventaireControlleur.jeterTicket(garePlusProche.getCouleurEvo(), 25);

                    //Il faut invalider le menu pour le mettre à jour
                    mainActivity.invalidateOptionsMenu();

                   dialog.cancel();
                   mettreAJourAffichage(gareControlleur, g, minDistance);
               }
            });
            dialog.setNegativeButton(R.string.boutonAnnuler, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // Refus, on ferme
                    dialog.cancel();
                }
            });

            //On va aussi changer la couleur du bouton
            boutonEvolution.setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
        } else
        {
            if(garePlusProche.getNiveau() == 0) {
                dialog.setMessage("Tamponner au moins 3 fois la gare pour pouvoir la faire passer au niveau 1");
            } else if(garePlusProche.getNiveau() >= 1 && garePlusProche.getNiveau() <= 2) {
                LayoutInflater inflater = mainActivity.getLayoutInflater();

                View viewDialog = inflater.inflate(R.layout.dialog_evolution, null);

                prepareDialogEvolution(viewDialog, garePlusProche, "Voici les ressources nécessaires pour passer la gare au niveau "+String.valueOf(garePlusProche.getNiveau() + 1));

                dialog.setView(viewDialog);
            }
            else {
                dialog.setMessage("Pas d'autre amélioration disponible");
            }

            //TODO : Mettre un bouton d'accord
            dialog.setPositiveButton(R.string.boutonValider, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //On ferme la boîte
                    dialog.cancel();
                }
            });

            //Et on remet la couleur de base du bouton
            boutonEvolution.clearColorFilter();
        }
        dialog.create();

        boutonEvolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }

    private void gestionBoutonFourniture(Gare garePlusProche, final ImageView boutonFournisseur)
    {
        //On ne peut prendre un ticket que si on est au moins niveau 1
        if(garePlusProche.getNiveau() > 0)
        {
            boutonFournisseur.setVisibility(View.VISIBLE);
            int color = Color.parseColor(garePlusProche.getCouleurString()); //The color u want
            boutonFournisseur.setColorFilter(color);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                boutonFournisseur.setImageAlpha(255);
            else
                boutonFournisseur.setAlpha(255);*/
            /*boutonFournisseur.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                        boutonFournisseur.setImageAlpha(55);
                    else
                        boutonFournisseur.setAlpha(55);
                }
            });*/
        }
        else
            boutonFournisseur.setVisibility(View.INVISIBLE);
    }

    private void mettreAJourAffichage(final GareCtrl gareControlleur, final Gare garePlusProche, final float minDistance)
    {
        TextView champ;
        //On récupère les boutons
        final Button boutonTamponner = (Button) mainActivity.findViewById(R.id.boutonTampon);
        final ImageView boutonFournisseur = (ImageView) mainActivity.findViewById(R.id.boutonFournisseur);
        final ImageView boutonEvolution = (ImageView) mainActivity.findViewById(R.id.boutonEvolution);

        if (garePlusProche == null) {
            champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheNom);
            champ.setText(R.string.aucuneGareProche);
            champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheNiveau);
            champ.setText(R.string.niveauNull);
            champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheDistance);
            champ.setText(R.string.naDistance);
            boutonTamponner.setEnabled(false);
            boutonFournisseur.setVisibility(View.GONE);
            boutonEvolution.setVisibility(View.INVISIBLE);
            ListView listView = (ListView) mainActivity.findViewById(R.id.listeGaresProches);
            listView.setAdapter(null);
        } else {
            // Create the adapter to convert the array to views
            GaresAdapter adapter = new GaresAdapter(mainActivity, listeGares);

            // Attach the adapter to a ListView
            ListView listView = (ListView) mainActivity.findViewById(R.id.listeGaresProches);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Gare gareTamponnee = listeGares.get(position);
                    Intent i = new Intent(mainActivity, GareActivity.class);
                    i.putExtra("IDGARE", gareTamponnee.getId());
                    mainActivity.startActivity(i);
                }
            });

            champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheNom);
            champ.setText(StringOutils.displayBeautifullNameStation(garePlusProche.getNom()));
	        champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheNiveau);
            champ.setText(mainActivity.getString(R.string.niveau) + " " + garePlusProche.getNiveau());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                champ.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheDistance);
            champ.setText((int) minDistance + " m");

            if (minDistance <= 150) {
                if(!tamponControlleur.ifAlreadyTamponned(garePlusProche.getId()))
                {
                    boutonTamponner.setEnabled(true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        champ = (TextView) mainActivity.findViewById(R.id.garePlusProcheNiveau);
                        champ.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                    }
                    final Gare finalGarePlusProche = garePlusProche;
                    boutonTamponner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //On tamponne
                            Tampon t = new Tampon(-1, finalGarePlusProche.getId(), new Date());
                            tamponControlleur.create(t);
                            boutonTamponner.setEnabled(false);

                            //On va au passage lui donner un ticket s'il peut en avoir un
                            if(garePlusProche.getNiveau() >= 1)
                                donnerTicket(garePlusProche.getCouleur());

                            //Et on regarde si on peut monter de niveau ou pas
                            gestionBoutonEvolution(gareControlleur, finalGarePlusProche, boutonEvolution, minDistance);
                        }
                    });
                }

                gestionBoutonEvolution(gareControlleur, garePlusProche, boutonEvolution, minDistance);
                gestionBoutonFourniture(garePlusProche, boutonFournisseur);
            } else {
                boutonTamponner.setEnabled(false);
                boutonFournisseur.setVisibility(View.GONE);
                boutonEvolution.setVisibility(View.GONE);
            }
        }
    }

    private void donnerTicket(int couleur) {
        inventaireControlleur.donnerTicket(couleur);

        mainActivity.invalidateOptionsMenu();
    }
}

public class RadarActivity extends AppCompatActivity {

    private static final int DEMANDE_DROIT_LOCALISATION = 1;
    public static final String PREFERENCE_ACTIVER_NOTIFICATION = "activerNotification";
    public static final String PREFERENCE_PRECEDENTE_VERSION = "derniereVersionMaJDialogue";
    private Intent serviceLocation;
    private boolean serviceEnCours = false;
    private InventaireCtrl inventaireCtrl;

    public Handler messageHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radar);

        checkDisplayUpdateDialog();

        TextView monTexte = (TextView) findViewById(R.id.garePlusProcheNom);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            monTexte.setText(R.string.localisationImpossible);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {

                    monTexte.setText(R.string.localisationEnCours);
                    messageHandler = new MessageHandler(this);
                    Log.d("LOCPG", "Demarrage du service");
                    serviceLocation = new Intent(this, LocationService.class);
                    serviceLocation.putExtra("MESSAGER", new Messenger(messageHandler));
                    serviceLocation.setAction("START");
                }
            } else {
                monTexte.setText(R.string.localisationEnCours);
                messageHandler = new MessageHandler(this);
                Log.d("LOCPG", "Demarrage du service");
                serviceLocation = new Intent(this, LocationService.class);
                serviceLocation.putExtra("MESSAGER", new Messenger(messageHandler));
                serviceLocation.setAction("START");
            }
        }

        inventaireCtrl = new InventaireCtrl(this);
    }

    private void checkDisplayUpdateDialog() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int lastVersionUpdate = preferences.getInt(RadarActivity.PREFERENCE_PRECEDENTE_VERSION, 0);
        if(lastVersionUpdate != BuildConfig.VERSION_CODE)
        {
            //On affiche la boîte de dialogue de mise à jour
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Version 1.1.8 :\n" +
                    "Cette nouvelle version voit l'ajout d'un système de ticket dans l'application.\n" +
                    "Chaque gare début au niveau 0. Après 3 validations, il est possible de la passer au niveau 1. Ainsi, elle vous délivera un ticket à chaque future validation.\n" +
                    "Les tickets servent à améliorer les gares au delà du niveau 1.\n" +
                    "Autres améliorations : \n" +
                    "- Nouveau mode d'affichage des tampons (disponible dans les paramètres)\n" +
                    "- Changement dans l'ergonomie de l'application\n" +
                    "- Corrections d'erreurs dans les données (noms de gares)")
                    .setTitle(R.string.dialogMiseAJourTitle);
            dialog.setPositiveButton(R.string.boutonDAccord, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            dialog.create();
            dialog.show();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(PREFERENCE_PRECEDENTE_VERSION, BuildConfig.VERSION_CODE);
            editor.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        if(serviceLocation == null) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, DEMANDE_DROIT_LOCALISATION);
        } else {
            if(!serviceEnCours) {
                startService(serviceLocation);
                serviceEnCours = true;
            }

            Message message = Message.obtain();
            message.arg1 = 2;
            messageHandler.sendMessage(message);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(serviceLocation != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            boolean notification = preferences.getBoolean(RadarActivity.PREFERENCE_ACTIVER_NOTIFICATION, true);
            if (notification) {
                Message message = Message.obtain();
                message.arg1 = 1;
                messageHandler.sendMessage(message);
            } else if (serviceEnCours) {
                stopService(serviceLocation);
                serviceEnCours = false;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Message message = Message.obtain();
        message.arg1 = 2;
        messageHandler.sendMessage(message);
        stopService(serviceLocation);
        inventaireCtrl.close();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case DEMANDE_DROIT_LOCALISATION: {
                // If request is cancelled, the result arrays are empty.
                TextView monTexte = (TextView) findViewById(R.id.garePlusProcheNom);
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Bon, bah, on demande la localisation
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    {
                        return;
                    }

                    monTexte.setText(R.string.localisationEnCours);
                    messageHandler = new MessageHandler(this);
                    serviceLocation = new Intent(this, LocationService.class);
                    serviceLocation.putExtra("MESSAGER", new Messenger(messageHandler));
                    serviceLocation.setAction("START");
                } else {
                    monTexte.setText(R.string.localisationImpossible);
                    return;
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_radar, menu);
        MenuItem menuItem = menu.findItem(R.id.voirTickets).setTitle(String.valueOf(inventaireCtrl.getTotalTicket()));

        menuItem.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()) {
            case R.id.voirTampons:
                i = new Intent(getApplicationContext(), ResumeVisaActivity.class);
                startActivity(i);
                return true;
            case R.id.voirTamponsDuJour:
                i = new Intent(getApplicationContext(), ResumeVisaActivity.class);
                i.putExtra("DUJOUR", true);
                startActivity(i);
                return true;
            case R.id.preferencesItem:
                i = new Intent(getApplicationContext(), PreferencesActivity.class);
                startActivity(i);
                return true;
            case R.id.voirTickets:
                i = new Intent(getApplicationContext(), MonnaieActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
