package fr.nocle.passegares;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collector;

import fr.nocle.passegares.adapter.TamponsAdapter;
import fr.nocle.passegares.controlleur.LigneCtrl;
import fr.nocle.passegares.controlleur.TamponCtrl;
import fr.nocle.passegares.modele.GareTamponnee;
import fr.nocle.passegares.modele.Ligne;

public class VisaActivity extends AppCompatActivity {
    static public final String PREFERENCE_NOUVELLE_INTERFACE_VISA = "nouvelleInterfaceVisa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visa);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        long idLigne = i.getLongExtra("IDLIGNE", 0);
        boolean voirTamponDuJour = i.getBooleanExtra("DUJOUR", false);

        TamponCtrl tamponControlleur = new TamponCtrl(this);

        //On récupère l'interface à afficher
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean nouvelleInterface = preferences.getBoolean(PREFERENCE_NOUVELLE_INTERFACE_VISA, true);

        Ligne l = null;
        final ArrayList<GareTamponnee> listeTampons = tamponControlleur.getByLine(idLigne, voirTamponDuJour, nouvelleInterface);

        if(idLigne == 0)
        {
            setTitle(getString(R.string.tousTampons) + (voirTamponDuJour ? " du jour" : "") + " (" + listeTampons.size() + ")");
        } else {
            LigneCtrl ligneControlleur = new LigneCtrl(this);
            l = ligneControlleur.get(idLigne);
            ligneControlleur.close();

            String precision;
            if(nouvelleInterface)
            {
                //On regarde ce qui est vraiment tamponné
                int nbGaresTamponnees = 0;
                for(GareTamponnee g : listeTampons)
                {
                    if(g.getNbValidations() > 0)
                        nbGaresTamponnees++;
                }
                precision = " (" + nbGaresTamponnees + "/" + listeTampons.size() + ")";
            }
            else
                precision = " (" + listeTampons.size() + ")";
            setTitle(l.getNom() + precision);
        }

        // Create the adapter to convert the array to views
        TamponsAdapter adapter = new TamponsAdapter(this, listeTampons, l);

        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.listeTampons);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GareTamponnee gareTamponnee = listeTampons.get(position);
                Intent i = new Intent(getApplicationContext(), GareActivity.class);
                i.putExtra("IDGARE", gareTamponnee.getIdGare());
                startActivity(i);
            }
        });

        tamponControlleur.close();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
