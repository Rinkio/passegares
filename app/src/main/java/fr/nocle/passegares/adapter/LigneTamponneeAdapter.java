package fr.nocle.passegares.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;

import fr.nocle.passegares.R;
import fr.nocle.passegares.modele.Gare;
import fr.nocle.passegares.modele.LigneTamponnee;
import fr.nocle.passegares.modele.Tampon;

/**
 * Created by jonathanmm on 01/10/16.
 */

public class LigneTamponneeAdapter extends ArrayAdapter<LigneTamponnee> {
    private Context context;
    private boolean voirTamponDuJour;

        public LigneTamponneeAdapter(Context context, ArrayList<LigneTamponnee> liste, boolean voirTamponDuJour)
        {
            super(context, 0, liste);
            this.context = context;
            this.voirTamponDuJour = voirTamponDuJour;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LigneTamponnee l = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.ligne_tamponnee_viewer, parent, false);
            }

            TextView champ;
            champ = (TextView) convertView.findViewById(R.id.nomLigne);
            champ.setText(l.getNomLigne());
            champ = (TextView) convertView.findViewById(R.id.nbTampons);
            if(voirTamponDuJour)
            {
                String tampon;
                if(l.getNbTampons() > 1)
                    tampon = convertView.getContext().getString(R.string.tampons);
                else
                    tampon = convertView.getContext().getString(R.string.tampon);

                champ.setText(l.getNbTampons()+" "+tampon);
            }
            else
                champ.setText(l.getNbTampons()+"/"+l.getNbGares());

            ImageView icon = (ImageView) convertView.findViewById(R.id.iconeLigne);

            switch (l.getNomLigne())
            {
                case "RER A":
                    icon.setImageResource(R.drawable.il_rer_a);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER B":
                    icon.setImageResource(R.drawable.il_rer_b);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER C":
                    icon.setImageResource(R.drawable.il_rer_c);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER D":
                    icon.setImageResource(R.drawable.il_rer_d);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "RER E":
                    icon.setImageResource(R.drawable.il_rer_e);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE H":
                    icon.setImageResource(R.drawable.il_transilien_h);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE J":
                    icon.setImageResource(R.drawable.il_transilien_j);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE K":
                    icon.setImageResource(R.drawable.il_transilien_k);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE L":
                    icon.setImageResource(R.drawable.il_transilien_l);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE N":
                    icon.setImageResource(R.drawable.il_transilien_n);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE P":
                    icon.setImageResource(R.drawable.il_transilien_p);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE R":
                    icon.setImageResource(R.drawable.il_transilien_r);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "LIGNE U":
                    icon.setImageResource(R.drawable.il_transilien_u);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M1":
                    icon.setImageResource(R.drawable.il_metro_m1);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M2":
                    icon.setImageResource(R.drawable.il_metro_m2);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M3":
                    icon.setImageResource(R.drawable.il_metro_m3);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M3b":
                    icon.setImageResource(R.drawable.il_metro_m3bis);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M4":
                    icon.setImageResource(R.drawable.il_metro_m4);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M5":
                    icon.setImageResource(R.drawable.il_metro_m5);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M6":
                    icon.setImageResource(R.drawable.il_metro_m6);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M7":
                    icon.setImageResource(R.drawable.il_metro_m7);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M7b":
                    icon.setImageResource(R.drawable.il_metro_m7bis);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M8":
                    icon.setImageResource(R.drawable.il_metro_m8);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M9":
                    icon.setImageResource(R.drawable.il_metro_m9);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M10":
                    icon.setImageResource(R.drawable.il_metro_m10);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M11":
                    icon.setImageResource(R.drawable.il_metro_m11);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M12":
                    icon.setImageResource(R.drawable.il_metro_m12);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M13":
                    icon.setImageResource(R.drawable.il_metro_m13);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "M14":
                    icon.setImageResource(R.drawable.il_metro_m14);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T1":
                    icon.setImageResource(R.drawable.il_tram_t1);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T2":
                    icon.setImageResource(R.drawable.il_tram_t2);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T3A":
                    icon.setImageResource(R.drawable.il_tram_t3a);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T3B":
                    icon.setImageResource(R.drawable.il_tram_t3b);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T4":
                    icon.setImageResource(R.drawable.il_tram_t4);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T5":
                    icon.setImageResource(R.drawable.il_tram_t5);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T6":
                    icon.setImageResource(R.drawable.il_tram_t6);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T7":
                    icon.setImageResource(R.drawable.il_tram_t7);
                    icon.setVisibility(View.VISIBLE);
                    break;
                case "T8":
                    icon.setImageResource(R.drawable.il_tram_t8);
                    icon.setVisibility(View.VISIBLE);
                    break;
                default:
                    icon.setVisibility(View.INVISIBLE);
                    break;
            }

            return convertView;
        }
}
