package fr.nocle.passegares.outils;

import fr.nocle.passegares.bdd.InventaireBDD;

/**
 * Created by jonathanmm on 19/12/16.
 */

public class CouleurOutils {
    static public String getHexa(int couleur) {
        switch(couleur)
        {
            case InventaireBDD.MONNAIE_ROUGE:
                return "#f44336";
            case InventaireBDD.MONNAIE_VIOLET:
                return "#9c27b0";
            case InventaireBDD.MONNAIE_CYAN:
                return "#00bcd4";
            case InventaireBDD.MONNAIE_LIME:
                return "#cddc39";
            case InventaireBDD.MONNAIE_JAUNE:
                return "#ffeb3b";
            case InventaireBDD.MONNAIE_ORANGE:
                return "#ff9800";
            case InventaireBDD.MONNAIE_MARRON:
                return "#795548";
            case InventaireBDD.MONNAIE_VERT:
                return "#4caf50";
        }

        return "#f44336"; //Couleur inconnue => rouge
    }
}
