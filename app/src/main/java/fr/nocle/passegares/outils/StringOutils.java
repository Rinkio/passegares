package fr.nocle.passegares.outils;

import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by jonathanmm on 11/12/16.
 */

public class StringOutils
{
    public static String displayBeautifullNameStation(String name, int limit)
    {
        String[] mots = name.split(" ");
        ArrayList<String> motsFormate = new ArrayList<>();
        boolean premierMot = true;
        ArrayList<String> particulesNonMajuscule = new ArrayList<String>();
        particulesNonMajuscule.add("de");
        particulesNonMajuscule.add("du");
        particulesNonMajuscule.add("la");
        particulesNonMajuscule.add("le");
        particulesNonMajuscule.add("et");
        particulesNonMajuscule.add("au");
        particulesNonMajuscule.add("a");
        for(String mot : mots)
        {
            String[] composants = mot.split("-");
            ArrayList<String> bouts = new ArrayList<>();
            for(String composant : composants)
            {
                String motFormate = "";
                int posPremiereLettre = 0;

                if(composant.substring(0, 1).equals("("))
                {
                    motFormate = "(";
                    posPremiereLettre = 1;
                } else if(composant.length() >= 2 && (composant.substring(1, 2).equals("'") || composant.substring(1, 2).equals("’")))
                {
                    motFormate = composant.substring(0, 1).toLowerCase() + "'";
                    posPremiereLettre = 2;
                }

                if(premierMot || !particulesNonMajuscule.contains(composant.toLowerCase()))
                    motFormate = motFormate + composant.substring(posPremiereLettre, posPremiereLettre + 1).toUpperCase() + composant.substring(posPremiereLettre + 1).toLowerCase();
                else
                    motFormate = motFormate + composant.toLowerCase();

                bouts.add(motFormate);

                if(premierMot)
                    premierMot = false;
            }
            motsFormate.add(TextUtils.join("-", bouts));
        }
        String phraseFormate = TextUtils.join(" ", motsFormate);
        if(limit != 0 && phraseFormate.length() > limit)
            return phraseFormate.substring(0, 30) + "…";
        else
            return phraseFormate;
    }

    public static String displayBeautifullNameStation(String name)
    {
        return displayBeautifullNameStation(name, 0);
    }
}
