package fr.nocle.passegares.controlleur;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.logging.Logger;

import fr.nocle.passegares.bdd.GareDansLigneBDD;
import fr.nocle.passegares.bdd.InventaireBDD;
import fr.nocle.passegares.bdd.LigneBDD;
import fr.nocle.passegares.bdd.TamponBDD;
import fr.nocle.passegares.modele.Gare;
import fr.nocle.passegares.bdd.GareBDD;
import fr.nocle.passegares.modele.Ligne;

/**
 * Created by jonathanmm on 03/09/16.
 */
public class GareCtrl extends Controlleur {
    public GareCtrl(Context contexte) {
        super(contexte);
        this.open();
    }

    public GareCtrl(SQLiteDatabase bdd)
    {
        super(bdd);
    }

    public void create(Gare g)
    {
        bdd.insert(GareBDD.TABLE_NOM, null, creerDepuisObj(g));
    }

    public void delete(long id)
    {
        bdd.delete(GareBDD.TABLE_NOM, GareBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(id)});
    }

    public void update(Gare g)
    {
        bdd.update(GareBDD.TABLE_NOM, creerDepuisObj(g), GareBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(g.getId())});
    }

    public Gare get(long id)
    {
        Cursor c = bdd.query(GareBDD.TABLE_NOM,
                new String[] {GareBDD.TABLE_ID_STIF, GareBDD.TABLE_NOM_GARE, GareBDD.TABLE_LONGITUDE, GareBDD.TABLE_LATITUDE, GareBDD.TABLE_EXPLOITANT, GareBDD.TABLE_NIVEAU, GareBDD.TABLE_COULEUR, GareBDD.TABLE_COULEUR_EVO},
                GareBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(id)}, null, null, null);
        c.moveToFirst();
        if(!c.moveToFirst())
            return null;
        Gare g = new Gare(id, c.getString(0), c.getString(1), c.getDouble(2), c.getDouble(3), c.getString(4), c.getInt(5), c.getInt(6), c.getInt(7));
        c.close();
        return g;
    }

    public Gare get(String idStif)
    {
        Cursor c = bdd.query(GareBDD.TABLE_NOM,
                new String[] {GareBDD.TABLE_CLE, GareBDD.TABLE_NOM_GARE, GareBDD.TABLE_LONGITUDE, GareBDD.TABLE_LATITUDE, GareBDD.TABLE_EXPLOITANT, GareBDD.TABLE_NIVEAU, GareBDD.TABLE_COULEUR, GareBDD.TABLE_COULEUR_EVO},
                GareBDD.TABLE_ID_STIF + " = ?", new String[] {String.valueOf(idStif)}, null, null, null);
        c.moveToFirst();
        if(!c.moveToFirst())
            return null;
        Gare g = new Gare(c.getLong(0), idStif, c.getString(1), c.getDouble(2), c.getDouble(3), c.getString(4), c.getInt(5), c.getInt(6), c.getInt(7));
        c.close();
        return g;
    }


    public ArrayList<Ligne> getCorrespondances(Gare g)
    {
        Cursor c = bdd.rawQuery("SELECT l.id, l.idStif, l.nom, l.type, l.ordre, l.couleur, l.nbGares " +
                "FROM Ligne l " +
                "INNER JOIN GareDansLigne gdl ON gdl.idLigne = l.id " +
                "WHERE gdl.idGare = ? " +
                "GROUP BY l.nom " +
                "ORDER BY l." + LigneBDD.TABLE_ORDRE + " ASC;", new String[] {String.valueOf(g.getId())});
        ArrayList<Ligne> listeLigne = new ArrayList<Ligne>();
        while (c.moveToNext()) {
            Ligne l = new Ligne(c.getLong(0), c.getString(1), c.getString(2), c.getString(3), c.getInt(4), c.getString(5), c.getInt(6));
            listeLigne.add(l);
        }
        c.close();
        return listeLigne;
    }

    public ArrayList<Gare> getNearlest(Location position)
    {
        Cursor c = bdd.query(GareBDD.TABLE_NOM, new String[] {GareBDD.TABLE_CLE, GareBDD.TABLE_ID_STIF, GareBDD.TABLE_NOM_GARE,
                GareBDD.TABLE_LONGITUDE, GareBDD.TABLE_LATITUDE, GareBDD.TABLE_EXPLOITANT, GareBDD.TABLE_NIVEAU, GareBDD.TABLE_COULEUR, GareBDD.TABLE_COULEUR_EVO},
                GareBDD.TABLE_LATITUDE + " >= ? AND " + GareBDD.TABLE_LATITUDE + " <= ? AND " + GareBDD.TABLE_LONGITUDE + " >= ? AND " + GareBDD.TABLE_LONGITUDE + " <= ?",
                new String[] {String.valueOf(position.getLatitude() - 0.01), String.valueOf(position.getLatitude() + 0.01), String.valueOf(position.getLongitude() - 0.015), String.valueOf(position.getLongitude() + 0.015)},
                null, null, null);
        ArrayList<Gare> listeGares = new ArrayList<Gare>();
        while (c.moveToNext()) {
            Gare g = new Gare(c.getLong(0), c.getString(1), c.getString(2), c.getDouble(3), c.getDouble(4), c.getString(5), c.getInt(6), c.getInt(7), c.getInt(8));
            listeGares.add(g);
        }
        c.close();
        return listeGares;
    }

    public static ContentValues creerDepuisObj(Gare g)
    {
        ContentValues valeur = new ContentValues();
        valeur.put(GareBDD.TABLE_ID_STIF, g.getIdStif());
        valeur.put(GareBDD.TABLE_NOM_GARE, g.getNom());
        valeur.put(GareBDD.TABLE_LONGITUDE, g.getLongitude());
        valeur.put(GareBDD.TABLE_LATITUDE, g.getLatitude());
        valeur.put(GareBDD.TABLE_EXPLOITANT, g.getExploitant());
        valeur.put(GareBDD.TABLE_NIVEAU, g.getNiveau());
        valeur.put(GareBDD.TABLE_COULEUR, g.getCouleur());
        valeur.put(GareBDD.TABLE_COULEUR_EVO, g.getCouleurEvo());
        return valeur;
    }

    public static ContentValues creerRelationGareLigne(long idGare, long idLigne)
    {
        ContentValues valeur = new ContentValues();
        valeur.put(GareDansLigneBDD.TABLE_ID_GARE, idGare);
        valeur.put(GareDansLigneBDD.TABLE_ID_LIGNE, idLigne);
        return valeur;
    }

    @Deprecated
    public static void initValues(Context contexte, SQLiteDatabase bdd)
    {
        ArrayList<Gare> listeGares = ImportCSV.importListGares(contexte, bdd);
        for(Gare g: listeGares)
        {
            long nouvelId = bdd.insert(GareBDD.TABLE_NOM, null, creerDepuisObj(g));
            g.setId(nouvelId);
            //Et on s'occupe aussi de noter la relation entre gare et ligne
            for(long idLigne: g.getIdLignes())
            {

                bdd.insert(GareDansLigneBDD.TABLE_NOM, null, creerRelationGareLigne(g.getId(), idLigne));
            }
        }
    }

    @Deprecated
    public static void updateValues(Context contexte, SQLiteDatabase bdd, int since, int to)
    {
        ArrayList<Gare> listeGares = ImportCSV.getToUpdateGares(contexte, bdd, since, to);
    }

    public static void updateAllLevels(Context contexte, SQLiteDatabase bdd)
    {
        Cursor c = bdd.rawQuery("SELECT g." + GareBDD.TABLE_CLE + ", count(t." + TamponBDD.TABLE_CLE + ") AS nbTampon, g." + GareBDD.TABLE_COULEUR + " couleur " +
                "FROM " + GareBDD.TABLE_NOM + " g " +
                "INNER JOIN " + TamponBDD.TABLE_NOM + " t ON g." + GareBDD.TABLE_CLE + " = t." + TamponBDD.TABLE_NOM_GARE + " " +
                "GROUP BY g." + GareBDD.TABLE_CLE + ";", null);
        int[] tickets = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; //12 couleurs possible
        while (c.moveToNext()) {
            long idGare = c.getLong(0);
            int nbTampons = c.getInt(1);
            int couleur = c.getInt(2);
            if(nbTampons >= 3) //On met à jour le niveau
            {
                ContentValues valeur = new ContentValues();
                valeur.put(GareBDD.TABLE_NIVEAU, 1);
                bdd.update(GareBDD.TABLE_NOM, valeur, GareBDD.TABLE_CLE + " = ?", new String[] {String.valueOf(idGare)});
                tickets[couleur] += nbTampons - 3;
            }
        }
        c.close();

        //Et on crédite les tickets, sans regarder la limite
        for(int couleur = 0; couleur < 12; couleur++)
        {
            ContentValues valeur = new ContentValues();
            valeur.put(InventaireBDD.TABLE_NOMBRE, tickets[couleur]);
            bdd.update(InventaireBDD.TABLE_NOM, valeur, InventaireBDD.TABLE_TYPE + " = " + InventaireBDD.TYPE_MONNAIE + " AND " + InventaireBDD.TABLE_ID_OBJ + " = ?", new String[] {String.valueOf(couleur)});
        }
    }
}
