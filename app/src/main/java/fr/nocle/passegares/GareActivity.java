package fr.nocle.passegares;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import fr.nocle.passegares.controlleur.GareCtrl;
import fr.nocle.passegares.controlleur.TamponCtrl;
import fr.nocle.passegares.modele.Gare;
import fr.nocle.passegares.modele.Ligne;
import fr.nocle.passegares.modele.Tampon;
import fr.nocle.passegares.outils.StringOutils;

public class GareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gare);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent i = getIntent();
        long idGare = i.getLongExtra("IDGARE", 0);

        GareCtrl gareControlleur = new GareCtrl(this);
        Gare gare = gareControlleur.get(idGare);

        TextView champ;

        champ = (TextView) findViewById(R.id.nomGare);
        champ.setText(StringOutils.displayBeautifullNameStation(gare.getNom()));

        champ = (TextView) findViewById(R.id.niveau);
        champ.setText(StringOutils.displayBeautifullNameStation(String.valueOf(gare.getNiveau())));

        //On gère les tickets
        if(gare.getNiveau() == 0)
        {
            GridLayout zoneTicket = (GridLayout) findViewById(R.id.zoneTicket);
            zoneTicket.setVisibility(View.GONE);
        } else {
            GridLayout zoneTicket = (GridLayout) findViewById(R.id.zoneTicket);
            zoneTicket.setVisibility(View.VISIBLE);

            int color = Color.parseColor(gare.getCouleurString());
            ImageView ticketFournit = (ImageView) findViewById(R.id.ticketFournitIcone);
            ticketFournit.setColorFilter(color);

            color = Color.parseColor(gare.getCouleurEvoString());
            ImageView ticketDemande = (ImageView) findViewById(R.id.ticketDemandeIcone);
            ticketDemande.setColorFilter(color);
        }

        champ = (TextView) findViewById(R.id.correspondances);
        ArrayList<Ligne> correspondances = gareControlleur.getCorrespondances(gare);
        ArrayList<String> nomCorrespondances = new ArrayList<>();
        for(Ligne l: correspondances)
        {
            nomCorrespondances.add(l.getNom());
        }
        champ.setText(TextUtils.join(", ", nomCorrespondances));

        TamponCtrl tamponControlleur = new TamponCtrl(this);
        ArrayList<Tampon> listeTampon = tamponControlleur.getTamponOfGare(gare);

        champ = (TextView) findViewById(R.id.nombreValidations);
        champ.setText(String.valueOf(listeTampon.size()));
        gareControlleur.close();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
